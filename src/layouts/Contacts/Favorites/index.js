import { useRef } from "react";
import { Icon, Placeholder } from "semantic-ui-react";
import ImageThumb from "../../../components/ImageThumb";
import './style.css';

const Favorites = ({favorites, loading}) => {

  const listRef = useRef(null);

  const scrollLeft = () => {
    if(listRef.current) {
      listRef.current.scrollBy({
        top: 0,
        left: 500,
        behaviour: "smooth",
      });
    }
  }

  const scrollRight = () => {
    if(listRef.current) {
      listRef.current.scrollBy({
        top: 0,
        left: -500,
        behaviour: "smooth",
      });
    }
  }

  const showIcons = () => {
    return favorites.length > 2
  }

  return (

    <div className="slide-container">
      { showIcons() && <Icon className="icon-class" name="caret left" size="large" onClick={() => {scrollLeft()}}></Icon> }
      { favorites.length > 0 && (
        <div className="items-container" ref={listRef}>
          {favorites.map((item) => {
            return (
              <div key={item.id} className="single-item-container">
                <ImageThumb 
                  first_name={item.first_name} 
                  last_name={item.last_name} 
                  src={item.contact_picture}
                  style={{width: 75, height: 75}}
                />
                <p className="name">
                  {item.first_name}
                  {item.last_name}
                </p>
              </div>
            )
          })}
        </div>)
      }
      { loading && (
          <Placeholder>
            <Placeholder.Header image>
              <Placeholder.Line />
              <Placeholder.Line />
            </Placeholder.Header>
          </Placeholder>
        )
      }
      { showIcons() && <Icon className="icon-class" name="caret right" size="large" onClick={() => {scrollRight()}}></Icon> }
    </div>
  )
}

export default Favorites;