import { Image } from "semantic-ui-react";
import './style.css'

const ImageThumb = ({first_name, last_name, src, className, style}) => {
  const getInitials= () => {
    return `${first_name[0]}${last_name[0]}`
  };

  return (
    <div>
      {src && 
        <Image
          circular
          heigth={45} 
          width={45}
          style={style}
          src={src}
          className={`thumbnail ${className}`}
        /> }

      {!src && (
        <div style={style} className={`thumbnail ${className}`}>
          <span>{getInitials()}</span>
        </div>
      )}
    </div>
  );
}

export default ImageThumb;